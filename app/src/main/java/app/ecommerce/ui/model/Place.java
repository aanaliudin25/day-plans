package app.ecommerce.ui.model;

public class Place {

    public int image;

    public Place(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
