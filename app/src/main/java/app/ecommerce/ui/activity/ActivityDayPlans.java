package app.ecommerce.ui.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.PlaceAdapter;
import app.ecommerce.ui.model.Place;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    RecyclerView recycler_view1;
    PlaceAdapter adapter1;
    RecyclerView recycler_view2;
    PlaceAdapter adapter2;
    LinearLayout detail1;
    LinearLayout detail2;
    LinearLayout show_detail;
    ImageView image_detail2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_plans_activity);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.grey_5));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.grey_5));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    private void initComponent() {
        // define recycler view
        recycler_view1 = findViewById(R.id.recycler_view1);
        recycler_view2 = findViewById(R.id.recycler_view2);
        show_detail = findViewById(R.id.show_detail);
        detail1 = findViewById(R.id.detail1);
        detail2 = findViewById(R.id.detail2);
        image_detail2 = findViewById(R.id.image_detail2);

        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler_view1.setLayoutManager(layoutManager1);
        recycler_view1.setHasFixedSize(true);
        recycler_view1.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager2
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler_view2.setLayoutManager(layoutManager2);
        recycler_view2.setHasFixedSize(true);
        recycler_view2.setNestedScrollingEnabled(false);

        List<Place> items1 = new ArrayList<>();
        items1.add(new Place(R.drawable.image_mosque_0));
        items1.add(new Place(R.drawable.image_mosque_1));
        items1.add(new Place(R.drawable.image_mosque_2));
        items1.add(new Place(R.drawable.image_mosque_3));
        items1.add(new Place(R.drawable.image_mosque_4));
        items1.add(new Place(R.drawable.image_mosque_5));
        items1.add(new Place(R.drawable.image_mosque_4));
        items1.add(new Place(R.drawable.image_mosque_3));

        List<Place> items2 = new ArrayList<>();
        items2.add(new Place(R.drawable.image_mosque_0));
        items2.add(new Place(R.drawable.image_mosque_5));
        items2.add(new Place(R.drawable.image_mosque_4));
        items2.add(new Place(R.drawable.image_mosque_3));
        items2.add(new Place(R.drawable.image_mosque_2));
        items2.add(new Place(R.drawable.image_mosque_1));
        items2.add(new Place(R.drawable.image_mosque_2));
        items2.add(new Place(R.drawable.image_mosque_3));

        //set data and list adapter
        adapter1 = new PlaceAdapter(this, items1);
        recycler_view1.setAdapter(adapter1);

        adapter2 = new PlaceAdapter(this, items2);
        recycler_view2.setAdapter(adapter2);

        detail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ActivityDayPlans.this, "No Action !", Toast.LENGTH_SHORT).show();
            }
        });

        detail2.setTag("ON");
        image_detail2.setImageResource(R.drawable.ic_keyboard_arrow_down);
        detail2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail2.getTag() == "ON") {
                    image_detail2.setImageResource(R.drawable.ic_keyboard_arrow_up);
                    show_detail.setVisibility(View.VISIBLE);
                    detail2.setTag("OFF");
                }else{
                    image_detail2.setImageResource(R.drawable.ic_keyboard_arrow_down);
                    show_detail.setVisibility(View.GONE);
                    detail2.setTag("ON");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_5));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
